# bash_profile for mac
# if [ -f ~/homebrew/etc/bash_completion.d/git-completion.bash ]; then
#   . ~/homebrew/etc/bash_completion.d/git-completion.bash
# fi
# if [ -f ~/homebrew/etc/bash_completion.d/git-prompt.sh ]; then
#   . ~/homebrew/etc/bash_completion.d/git-prompt.sh
# fi

# export PATH="$HOME/homebrew/bin:$PATH"

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

Color_Off='\[\e[0m\]'   # Text Reset
Green='\[\e[0;32m\]'    # Green
Yellow='\[\e[0;33m\]'   # Yellow
Cyan='\[\e[0;36m\]'     # Cyan
#PS1='\[\e[0;32m\]\u@\h\[\e[m\] \[\e[0;36m\]\W\[\e[m\] \[\e[1;32m\]\$\[\e[m\] \[\e[0;37m\]'
PS1="${Green}\u@\h$Yellow\$(__git_ps1) $Cyan\W $Green\$ $Color_Off"

# alias
alias ..='cd ..'
alias ls='ls -G'
alias l='ls -rlt'
alias l.='ls -d .*'
alias ll='ls -l'
alias clc='clear; l'
alias cp='cp -i'
alias mc='mv -i'
alias mv='mv -i'
alias rm='rm -i'
alias grep='grep --color=auto'
alias hist='history | grep'
alias vim='mvim -v'
alias vimdiff='mvimdiff -v'

# source /opt/intel/composer_xe_2013/bin/compilervars.sh intel64
