#
export PATH=$HOME/homebrew/bin:$PATH
export EDITOR='vim'
# external Plugins {{{
source ~/homebrew/Cellar/zsh-syntax-highlighting/0.2.1/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/homebrew/Cellar/zsh-history-substring-search/1.0.0/zsh-history-substring-search.zsh
# }}}
#
#  keybinding {{{
bindkey -e
zmodload zsh/terminfo
bindkey '^[[A' history-substring-search-up # up arrow
bindkey '^[[B' history-substring-search-down # down arrow
zmodload zsh/complist
bindkey '^[[Z' reverse-menu-complete # shift tab -> reverse completion
bindkey '\e[3~' delete-char # Del
bindkey '^H' backward-word
bindkey '^L' forward-word
bindkey '^[[H' beginning-of-line # home key
bindkey '^[[F' end-of-line # end key
#  }}}
#
#  completion {{{
setopt no_complete_aliases # Actually: completes aliases!
#  }}}
#
#  alias {{{
alias ..='cd ..'
alias ls='ls -G'
alias l='ls -rlt'
alias l.='ls -d .*'
alias ll='ls -l'
alias clc='clear; l'
alias cp='cp -i'
alias mc='mv -i'
alias mv='mv -i'
alias rm='rm -i'
alias grep='grep --color=auto'
alias hist='history | grep'
alias vim='mvim -v'
alias vimdiff='mvimdiff -v'
#  }}}
