#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# auto complete using sudo
complete -cf sudo
# GIT
source ~/.git-completion.bash
source ~/.git-prompt.sh

# remove orphans if found
orphans() {
  if [[ ! -n $(pacman -Qdt) ]]; then
    echo "No orphans to remove."
  else
    sudo pacman -Rs $(pacman -Qdtq)
  fi
}

export GIT_PS1_SHOWDIRTYSTATE=1

# Colors
C_Off='\[\e[0m\]'       # reset
White='\[\e[0;37m\]'    # White
Red='\[\e[0;31m\]'      # Red
BRed='\[\e[1;31m\]'     # Bold Red
Green='\[\e[0;32m\]'    # Green
BGreen='\[\e[1;32m\]'   # Bold Green
Yellow='\[\e[0;33m\]'   # Yellow
Cyan='\[\e[0;36m\]'     # Cyan
FancyX='\342\234\227'
Checkmark='\342\234\223'

#default colors
#PS1='[\u@\h \W]\$ '
#green colors
# export PS1="$Green\u@\h$Yellow\$(__git_ps1) $Cyan\W $BGreen\$ $C_Off"

set_prompt () {
    Last_Command=$? # Must come first!

    # Add a white exit status for the last command
    PS1="$White\$? "
    # If it was successful, print a green check mark. Otherwise, print
    # a red X.
    if [[ $Last_Command == 0 ]]; then
        PS1+="$BGreen$Checkmark "
    else
        PS1+="$BRed$FancyX "
    fi
    # If root, just print the host in red. Otherwise, print the current user
    # and host in green.
    if [[ $EUID == 0 ]]; then
        PS1+="$Red "
    else
        PS1+="$Green\\u "
    fi
    # Print the working directory and prompt marker in blue, and reset
    # the text color to the default.
    PS1+="$Yellow\$(__git_ps1) $Cyan\\W $BGreen\$$C_Off "
}
PROMPT_COMMAND='set_prompt'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias l='ls -rlt'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias clc='clear; l'
alias rm='rm -f'
alias vi='vim'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
alias grep='grep --color=auto'
alias hist='history | grep '

cdd (){
    cd $1
    clear
    pwd
    l
}

# shutdown
# alias reboot="sudo reboot"
# alias shutdown="shutdown -h now"

# export EDITOR="vim"
